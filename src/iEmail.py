import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(json_data):
  word = json_data['search_word']
  comment = json_data['comment']
  text =  """\
    Hi! This is the notification that your mapped word is appearing"""

  def create_subject(word):
    return "Mapped Word Occurrency: {word}".format(word= word)
  
  # Create the plain-text and HTML version of your message

  def get_html(word, comment):
    return  """\
    <html>
      <body>
        <p>Hello,<br>
          Your mapped word {word} in the comment \n
          {comment}
        </p>
      </body>
    </html>
    """.format(word = word, comment=comment)
    
  sender_email = "bpnotifyautomation@gmail.com"
  receiver_email = "bpnotifyautomation@gmail.com"
  password = "Bp@123!!"
  message = MIMEMultipart("alternative")
  message["From"] = sender_email
  message["To"] = receiver_email
  message["Subject"] = create_subject(word)


  # Turn these into plain/html MIMEText objects
  part1 = MIMEText(text, "plain")
  part2 = MIMEText(get_html(word, comment), "html")

  # Add HTML/plain-text parts to MIMEMultipart message
  # The email client will try to render the last part first
  message.attach(part1)
  message.attach(part2)

  # Create secure connection with server and send email
  context = ssl.create_default_context()
  with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(
      sender_email, receiver_email, message.as_string()
    )