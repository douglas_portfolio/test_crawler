# %%
import json
import requests as req
from bs4 import BeautifulSoup
import pymongo
from pymongo import MongoClient
import uuid
from pylint.checkers import newstyle


url = 'https://news.ycombinator.com/newcomments'
response = req.get(url)

# Database connection

cluster = pymongo.MongoClient(
    "mongodb+srv://atlasUser:123mudar@brasilparalelocluster.ooe97.mongodb.net/hncol?retryWrites=true&w=majority")
db = cluster.test

# Comment me when finish
collection = cluster['hacker_news_db']
document = collection['hncol']

# Create Method to push data in mongo

def create_content(comment):
    return {
        'text': comment
    }

# Find elements


soup = BeautifulSoup(response.content, "lxml")
        
for item in soup.find_all('div', {"class", 'comment'}):
    comments = item.select('.commtext')
    if comments:
        item = comments[0].get_text()
        post = create_content(item)
        document.insert_one(post)
        
print("Database Updated")
