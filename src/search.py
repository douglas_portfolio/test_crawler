# %%
import json
import requests as req
from bs4 import BeautifulSoup
import pymongo
from pymongo import MongoClient
import iEmail
# Database connection


cluster = pymongo.MongoClient(
    "mongodb+srv://atlasUser:123mudar@brasilparalelocluster.ooe97.mongodb.net/hncol?retryWrites=true&w=majority")
db = cluster.test

# Comment me when finish
cluster.drop_database('hncol')

database = cluster['hacker_news_db']
allComentsColl = database['hncol']
foundCommentsCollection = database['found']


for c in database.list_collection_names():
    print(c)


def get_document_with_value_in_text(coll, value):
    return{
        "cursors": coll.find({'text': {'$regex': value, '$options': 'i'}}),
        "word": value
    } 

found_by_crawler = get_document_with_value_in_text(allComentsColl, 'linux')

print("All found:")
search_value = found_by_crawler['word']
for document in found_by_crawler['cursors']:
    comment = document['text']
    cursorInFound = get_document_with_value_in_text(foundCommentsCollection, comment)
    if cursorInFound['cursors'].count() == 0:
         foundCommentsCollection.insert_one({"text": comment})
         print(comment)
        
    else:
     iEmail.send_email({
         "comment": comment,
         "search_word": search_value
     })
