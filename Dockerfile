FROM ubuntu
RUN apt-get update
RUN apt-get install git -y
RUN apt-get install python3 -y
RUN apt install python3-pip -y
RUN mkdir /bin/crawler
RUN mkdir /bin/search
COPY run.sh /bin/crawler/run.sh
COPY search.sh /bin/crawler/run.sh
COPY root /var/spool/cron/crontabs/root
RUN chmod 777 /bin/crawler/run.sh
RUN chmod 777 /bin/search/search.sh
CMD crond -l 2 -f