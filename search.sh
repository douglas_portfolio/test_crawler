#!/bin/bash
timestamp=`date +%Y/%m/%d-%H:%M:%S`
echo "System path is $PATH at $timestamp"
cd /bin/search
git clone http://dsbonafe@bitbucket.org/douglas_portfolio/test_crawler.git
cd test_crawler
pip3 install poetry
poetry install
python ./src/search.py